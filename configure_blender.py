# Apply all my standard customisations to a new Blender install
#
# Author: Aligorith
# Date: July 2018
# Versions: 2.7x/2.8

import bpy
import os

# Context
C = bpy.context


# Version - 2.8 or 2.7?
version = bpy.app.version
version_string = "%s.%s" % (version[0], version[1])

is_b28  = (version[0] == 2) and (version[1] >= 80)


# Determine user-settings directory
# NOTE: Macs are not supported, and will not be supported.
if os.name == 'nt':
	# Windows
	USER_SETTINGS_PATH = "~\\AppData\\Roaming\\Blender Foundation\\Blender\\%s\\" % (version_string)
else:
	# Linux (or maybe Mac too), but assume it's Linux
	USER_SETTINGS_PATH = "~/.config/blender/%s/" % (version_string)

USER_SETTINGS_DIR = os.path.expanduser(USER_SETTINGS_PATH)


# User Preferences ===============================
U = C.user_preferences

# Show Quit Dialog - Off   --> This is really annoying when testing
U.view.use_quit_dialog = False

# Show Python tooltips - On  --> As a dev, it's important to show them
U.view.show_tooltips_python = True

# UI Scale - 1.1 to make it easier to see things
U.view.ui_scale = 1.1

# Navigate Manipulator (2.8 only) --> Gets in the way
if hasattr(U.view, "mini_axis_type"):
	U.view.mini_axis_type = 'MINIMAL'
elif hasattr(U.view, "show_manipulator_navigate"):
	U.view.show_manipulator_navigate = False
else:
	print("Bulky navigate manipulator skipped")

# ---------------------------------

# FCurve Visibility --> Bump up the visibility to make them easier to work with
U.edit.fcurve_unselected_alpha = 0.82

# XYZ to RGB --> Off, since the (old/2.5) theme colors for axes are bad
U.edit.use_insertkey_xyz_to_rgb = False

# Use Negative Frames --> On - It's so much more convenient to be able to do it!
U.edit.use_negative_frames = True

# GP Default Color --> Green
if not is_b28:
	U.edit.grease_pencil_default_color = (0.497, 0.735, 0.167, 0.9)

# Sculpt Paint Overlay Color -> XXX: Black is not good
#U.edit.sculpt_paint_overlay_color = (...)

# ---------------------------------

# Turntable Orbit - Set this in case someone changes it in future!
U.inputs.view_rotate_method = 'TURNTABLE'

# Invert mousewheel direction -> it feels more natural to push/pull the view 
# towards/away from yourself, instead of adjusting the "zoom level"
U.inputs.invert_zoom_wheel = True


# ---------------------------------

# Auto-Run Python Scripts ->  Blender rigs are useless without this!
U.system.use_scripts_auto_execute = True

# Reset "temp" dir to be reusable
U.filepaths.temporary_directory = "/tmp/"

# Only save a single old version of the file
# XXX: This is the default... don't need more for now
U.filepaths.save_version = 1

# Don't autosave files -> Wastes too much diskspace, when you have a habit of saving anyway
U.filepaths.use_auto_save_temporary_files = False

# Bump up the number of recent files -> Makes it easy to keep all your testfiles handy
U.filepaths.recent_files = 25

# ---------------------------------

# Enable multisampling -> for nicer looking lines
U.system.multi_sample = '4'


# Theme Tweaks ===================================

# Default Theme
theme = U.themes[0]


# Icon Saturation --> Full... Desaturated is UGLY!
if is_b28:
	theme.user_interface.icon_saturation = 1.0

# Camera/Light colors
if is_b28:
	theme.view_3d.camera = (0.34117648005485535, 0.5254902243614197, 0.7254902124404907)
	theme.view_3d.light  = (0.803921639919281, 0.6627451181411743, 0.0, 0.1568627506494522)
else:
	theme.view_3d.camera = (0.34117648005485535, 0.5254902243614197, 0.7254902124404907)
	theme.view_3d.lamp   = (0.803921639919281, 0.6627451181411743, 0.0, 0.1568627506494522)


# Keymaps ========================================

wm = C.window_manager
kc = wm.keyconfigs.user

# Restore changes to the default 2.8 keymap that are annoying/hard to work with
if is_b28:
	km = kc.keymaps['Window']
	
	# Open File - F1
	# NOTE: Replaces the useless "open manual" command
	idx = km.keymap_items.find("wm.doc_view_manual_ui_context")
	if idx != -1:
		kmi = km.keymap_items[idx]
		km.keymap_items.remove(kmi)
	
	kmi = km.keymap_items.new('wm.open_mainfile', 'F1', 'PRESS')
	
	# Save File - Ctrl-W   (Note: Less essential... can stick with Ctrl-S)
	#km = kc.keymaps['Window']
	
	# Select All -> Toggle Select All
	 

# Search - ???

# (Play - Spacebar)


# Default Settings ===============================

# Active Scene
scene = C.scene



# Don't save to /tmp/, especailly not on windows!
scene.render.filepath = "//render/"

# Reduced resolution
scene.render.resolution_percentage = 75

# Set higher compression
scene.render.image_settings.compression = 90 


# Viewport/Editor Settings ========================

# 3D View
#   - Disable Motion Tracking stuff
#   - Disable manipulators

for screen in bpy.data.screens:
	for area in screen.areas:
		for ed in area.spaces:
			if ed.type == 'VIEW_3D':
				# Disable motion tracking
				ed.show_reconstruction = False
				
				# Disable transform manipulators?
				#ed.show_manipulator = False

# Bookmarks =======================================

BOOKMARKS_PATH = os.path.join(USER_SETTINGS_DIR, "bookmarks.txt")
if not os.path.exists(BOOKMARKS_PATH):
	with open(BOOKMARKS_PATH, 'w') as f:
		f.write("[Bookmarks]\n")
		f.write("%s\n" % os.path.join(os.path.expanduser("~"), "Downloads"))
		# TODO: Testfiles - Location depends on machine used though
