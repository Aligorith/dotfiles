dotfiles
========

This repository contains copies of all the configuration files/addons/templates/etc.
that are needed to set up new machine to my tastes. The stuff here doesn't contain
any of the really sensitive stuff (e.g. passwords + keys); all other things that can
be automated though will be added in due course.

It is intended that the helper scripts will only be run once the software they affect
have been installed.

Getting This Repo
-----------------

This repo makes use of *git submodules* (in particular, for the Sublime Text packages
it will install automatically). Therefore, to get a copy of this repo, use the following
commands.

**With Push Access** (me):
`
$ git clone --recurse-submodules git@github.com:Aligorith/dotfiles.git
`

**Without Push Access** (everyone else):
`
$ git clone --recurse-submodules https://github.com/Aligorith/dotfiles.git
`

Usage/Installation
------------------

1) Verify for yourself that the scripts you'll be running aren't doing anything nefarious
   AND that they'll write files to the right locations. If not, fix the scripts first.

2) Using an elevated Powershell, run the following line so that we can run the scripts here
   `$ Set-ExecutionPolicy RemoteSigned`
   This *should* have the effect of making it so that it'll allow running any scripts created.

3) Run `install_dotfiles.ps1` to copy the dotfiles and config files to the right directories

4) Manually copy over/apply "secrets" back into the relevant files.
   (**TODO**: Build scripts to do this)

5) Run `fix_win10.ps` to apply general privacy/customisation fixes for Windows 10  
   (**TODO**: adapt from the existing privacy-enhancing scripts)


Assumptions
-----------

This repo is intended for my personal use only. It makes the following assumptions:
 * All the packages being used have been installed already.
   (**TODO**: Automate the fetching and installation of these too)
 * Most tools are installed to "C:\Apps" - Including FFMPEG, HexChat, Krita, MuseScore,
   Sublime Text, VLC, Youtube-DL


Additional Modules
------------------

Not all the directories listed are installed automatically. Some still require manual
intervention (e.g. they act as a template to be used across multiple repos).

 * **blender__dev__workflow** - A bunch of utility scripts/settings used to make it easier
   to develop Blender on Windows. These currently need to be manually applied to per checkout.
 
 * **cmdutils** - Command line utilities (actually, wrappers around some basic/common tools)
   to make them easier to use. This is not currently "installed" by default as I'm still evaluating
   whether they should be kept.

