#!/usr/bin/env bash
#
# Script to download an initialise a new Blender repository
# Author: Joshua Leung (aligorith@gmail.com)
# Date: April 2018

# Configuration Parameters --------------------------------
# NOTE: Customise these before running the script

ROOT_DIR=$PWD
REPO_DIR=$1  # first parameter is the name of the repo to use

BUILD_DIR="build_linux"  # XXX: figure out how to make cmake write to just "build"

AS_DEV=false
OPTIMISE_REBUILDS=true

if [ $# = 2 ]; then
	# optional second parameter is either "--install_deps" or "<branchname>"
	if [ $2 = "--install_deps" ]; then
		echo " -- Install Deps"
		INSTALL_DEPS=true
		BRANCH="master"
	else
		echo " -- Checkout Branch '$2'"
		INSTALL_DEPS=false
		BRANCH=$2
	fi
else
	# no (extra) arguments provided - do nothing
	INSTALL_DEPS=false
	BRANCH="master"
fi


# Printing Helpers (from blender's install_deps.sh) --------------------------

BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
LIME_YELLOW=$(tput setaf 190)
POWDER_BLUE=$(tput setaf 153)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
BRIGHT=$(tput bold)
NORMAL=$(tput sgr0)
BLINK=$(tput blink)
REVERSE=$(tput smso)
UNDERLINE=$(tput smul)

_echo() {
  if [ "X$1" = "X-n" ]; then
     shift; printf "%s" "$@"
  else
     printf "%s\n" "$@"
  fi
}

ERROR() {
  _echo "${BRIGHT}${RED}ERROR! ${NORMAL}${RED}$@${NORMAL}"
}

WARNING() {
  _echo "${BRIGHT}${YELLOW}WARNING! ${NORMAL}${YELLOW}$@${NORMAL}"
}

USAGE() {
  _echo "${BRIGHT}${YELLOW}USAGE! ${BRIGHT}${YELLOW}$@${NORMAL}"
}

INFO() {
  _echo "${BRIGHT}${GREEN}$@${NORMAL}"
}

PRINT() {
  _echo "$@"
}


# Help/Usage Info -----------------------------------------

if [ $# = 0 ]; then
	USAGE "$ . brepo_init.sh <repo_name> --install_deps | <branch_name>]"
	echo "($# args found)"
	#return
fi


# Let the magic begin --------------------------------------

# TODO: Check if root directory exists
if [ $# = 0 ]; then
	REPO_DIR="master"
fi
REPO_ROOT_DIR="$ROOT_DIR/$REPO_DIR"


INFO "Creating repo - '$REPO_ROOT_DIR'"
mkdir $REPO_ROOT_DIR
cd $REPO_ROOT_DIR

INFO "Checking out Blender Sources..."
if [ $AS_DEV = true ]; then
	git clone git@git.blender.org:blender.git
else
	git clone git://git.blender.org/blender.git
fi

cd blender

INFO "Checking out Blender Submodules..."
git submodule update --init --recursive
git submodule foreach git checkout master
git submodule foreach git pull --rebase origin master

if [ "$BRANCH" != "master" ]; then
	INFO "Checking out '${BRANCH}' branch..." 
	git checkout -b $BRANCH origin/$BRANCH
fi

# Install dependencies -------------------------------------

if [ $INSTALL_DEPS = true ]; then
	INFO "Installing dependencies..."
	build_files/build_environment/install_deps.sh
fi

# Make Symlinks ------------------------------------------

INFO "Making links..."
mkdir ../build_linux   # XXX: this is the name used by the default makefile
mkdir ../build_linux/bin

ln -s "$REPO_ROOT_DIR/build_linux/bin/" "$REPO_ROOT_DIR/install"

# Optimise rebuilds
if [ $OPTIMISE_REBUILDS = true ]; then
	INFO "  Optimise rebuilds..."
	ln -s "$REPO_ROOT_DIR/blender/release" "$REPO_ROOT_DIR/build_linux/bin"
fi

# Done ---------------------------------------------------
# XXX: Ideally we would be able to help the user change current directories in the parent shell too...
# For now, just copy the text to the clipboard
INFO "Done! Change current directory to '$REPO_ROOT_DIR/blender' to compile"



