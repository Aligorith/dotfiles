

# Prompt Text - Multiline, to avoid things going too long
PS1='\n${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\] (\T): \[\033[01;36m\][[\w]]\[\033[00m\]\n$ '

# Logitec K120 "US Intl" edition fix - Disable bar-key beside left shift key
xmodmap -e "keycode 94 = Shift_L"

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

