@echo off
@REM Script to setup a new Blender repo ready for use
@REM (this is the Windows version of a similar script used for Linux)

setlocal ENABLEEXTENSIONS

set BLENDER_ROOT=%cd%

set AS_DEV=1
set OPTIMISE_REBUILDS=1
set INSTALL_DEPS=0
set BRANCH_NAME=master

if NOT "%1" == "" (
	set REPO_DIR=%1
	if NOT "%2" == "" (
		if "%2" == "--install_deps" (
			set INSTALL_DEPS=1
		) else (
			set BRANCH_NAME=%2
		)
	)
) else (
	set REPO_DIR=master2
)

@REM Ensure directories exist
@echo Creating repo directory - %REPO_DIR%
if NOT EXIST %REPO_DIR% (
	mkdir %REPO_DIR%
)
cd %REPO_DIR%

if NOT EXIST "blender" (
	@echo Checkout out source code
	if "%AS_DEV%" == "1" (
		git clone git@git.blender.org:blender.git
	) else (
		git clone git://git.blender.org/blender.git
	)
)

cd blender
@echo Checking out Blender Submodules...
git submodule update --init --recursive
@REM git submodule update --remote

if NOT "%BRANCH_NAME%" == "master" (
	@echo Checking out %BRANCH_NAME%
	git checkout -b %BRANCH_NAME% origin/%BRANCH_NAME%
	git submodule update --remote
)

if "%INSTALL_DEPS%" == "1" (
	@echo Installing dependencies...
	cd %BLENDER_ROOT%
	
	mkdir lib
	cd lib

	svn checkout https://svn.blender.org/svnroot/bf-blender/trunk/lib/windows_vc14
)

@echo Making links...
cd %BLENDER_ROOT%\%REPO_DIR%
mkdir build
mkdir build\bin
mkdir build\bin\ReleaseWithDebInfo

mklink /D install build\bin\ReleaseWithDebInfo
mklink /D lib %BLENDER_ROOT%\lib

@echo Ready to compile. Just type "make"
cd blender


