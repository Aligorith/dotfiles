@echo off
@REM Set command-line prompt to my custom one
@REM (NOTE: To make this persist, manually apply/set this as an environment variable)

set prompt=$_---[$P]---$_$T$h$h$h$G 
