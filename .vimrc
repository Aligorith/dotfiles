" Linux version of Vim Config

" decent sized window
set lines=40 columns=120

" indention
":set cpoptions += I
set autoindent
set indentexpr=GetIndent()

function GetIndent()
   let lnum = prevnonblank(v:lnum - 1)
   let ind = indent(lnum)
   return ind
endfunction

set backspace=2 " make backspace work like other apps
" XXX: set backspace=indent,eol,start

" tabs must be used, and of size 4
set ts=4
set sw=4        " shiftwidth sounds like it will insert spaces... otherwise make this 4
set pi            " preserve indent always

" nicer search behaviour
set ignorecase  " can do lowercase queries
set smartcase   " typing with some uppercase won't screw things up
set incsearch   " jumps to matches as you type - seems to be default already

set showmatch   " highlight matching brackets

" line numbers
set number      " line numbers are always shown
set cursorline  " highlight current line

" word wrapping
set wrap
set linebreak
set nolist   " list disables linebreak

set backupdir=~/vim_tmp   " it sucks to have to keep cleaning up temp files all over the show...
set directory=~/vim_tmp

inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>

" spell-checking - "GB" to make it nicer when doing thesis writing (should actually use nz)
set spell spelllang=en_nz

syntax on                           " syntax highlighing
filetype on                          " try to detect filetypes
filetype plugin indent on    " enable loading indent file for filetype


" Vim Latex ==========================
" REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
filetype plugin on

" IMPORTANT: win32 users will need to have 'shellslash' set so that latex
" can be called correctly.
set shellslash

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" OPTIONAL: This enables automatic indentation as you type.
filetype indent on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

" Use PDF Latex
let g:Tex_DefaultTargetFormat='pdf'

" No folding - it makes things too tricky
let Tex_FoldedSections=""
let Tex_FoldedEnvironments=""
let Tex_FoldedMisc=""

" End of Vim Latex ===================

" Move around the wrapped lines as they appear, not as they are - makes editing easier
noremap  <buffer> <silent> k gk
noremap  <buffer> <silent> j gj
noremap  <buffer> <silent> 0 g0
noremap  <buffer> <silent> $ g$

" (normal)
noremap  <buffer> <silent> <Up> gk
noremap  <buffer> <silent> <Down> gj
" (insert)
inoremap  <buffer> <silent> <Up> <C-O>gk
inoremap  <buffer> <silent> <Down> <C-O>gj


" Copy and Paste using normal keys
vnoremap <C-c> "*y
vnoremap <C-S-v> "*p

" set gfn=Consolas:h10:cANSI


