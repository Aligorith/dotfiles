$HOME = $env:USERPROFILE

write "Copying PowerShell configuration defaults..."
cp -r WindowsPowerShell $HOME\Documents\WindowsPowerShell 

write "Copying dotfiles..."
cp .gitconfig $HOME\.gitconfig
cp .git $HOME\.gitk
# XXX: inputrc needs to be on path, so maybe needs to be in another folder?
cp .inputrc $HOME\.inputrc

write "Copying app configs..."

write '  Copying => "Audacity"'
# XXX: perhaps we shouldn't write every section of the config files?
cp -r Audacity $HOME\AppData\Roaming\Audacity

write '  Copying => "Inkscape"'
#cp -r Inkscape $HOME\AppData\Roaming\Inkscape

write '  Copying => "MuseScore"'
cp -r MuseScore\local_appdata $HOME\AppData\Local\MuseScore\MuseScore2
cp -r MuseScore\roaming_appdata $HOME\AppData\Roaming\MuseScore
cp -r MuseScore\user_settings $HOME\Documents\MuseScore2

write '  Copying => "Sublime Text 3"'
cp -r ST3 "$HOME\AppData\Roaming\Sublime Text 3"

write '  Copying => "VIM"'
cp _vimrc $HOME\_vimrc
cp -r vimfiles $HOME\vimfiles
mkdir $HOME\vim_tmp > $null

write '  Copying => "VLC"'
cp -r VLC $HOME\AppData\Local\Roaming\vlc

write '  Copying => "Wacom Tablet Settings"'
cp intuos_pro5.wacomprefs $HOME\intuos_pro5.wacomprefs

write '  Copying ==> "Darktable" Settings'
cp .\Darktable\darktable.css $HOME\AppData\Local\darktable\darktable.css


write 'Done!'





