@echo off

echo PyQt
pip install pyqt5

echo
echo PyOpenGL
pip install pyopengl

echo
echo Pillow
pip install pillow

echo
echo scipy + numpy
pip install scipy

echo
echo matplotlib
pip install matplotlib

echo
echo pandas
pip install pandas

@REM echo
@REM echo json5
@REM pip install json5

echo
echo fbs (pyqt standalone maker)
pip install fbs

echo
echo Pelican
pip install pelican

echo
echo Markdown
pip install markdown



