#write "Loading PS modules..."
#Import-Module SetConsoleFont

# Custom Prompt Override
#
# For example (indented 2 spaces):
#   ---[C:\Users\Aligorith\Documents]---
#   P 14:53:00 > 
#
function prompt {
	# `r`n inserts a newline
	"`r`n---[$(Get-Location)\]---`r`nP $(Get-Date -Format 'HH:mm:ss') > "
}

# Appearance
write "Initialising Defaults..."
$console = $host.ui.RawUI

# Get a decent font size
# (This seems to set Lucida 14)
#Set-ConsoleFont 8

# TODO: Override system colors
#$console.ForegroundColor = "white"
#$console.BackgroundColor = "gray"


# Buffer Size
$buffer = $console.BufferSize
$buffer.Width = 130
$buffer.Height = 9999
$console.BufferSize = $buffer

# Clear Screen, Ready for work
Clear-Host
write "PowerShell...`r`n"

