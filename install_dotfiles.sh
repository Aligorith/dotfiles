# Utility script to install all dotfiles relevant to Linux

# Remap Logitec K120 "US Intl" left-bar key beside shift (that causes problems)
echo "Fixing Logitec K120 'US Intl' extra bar-key problem..."
# TODO: Append the following line to .bashrc
#xmodmap -e "keycode 94 = Shift_L"

# InputRC (tab completion/cycling)
echo "Setting up InputRC (tab cycling)..."
cp .inputrc ~/.inputrc

# Vim -----------------------
echo "Setting up Vim"
cp .vimrc ~/.vimrc
mkdir ~/vim_tmp

cp -R .vim ~/.vim
cp -R vimfiles ~/.vim

# Git -----------------------
echo "Setting up Git..."
cp .gitconfig ~/.gitconfig

git config --global core.autocrlf false

# TODO: Fix font config
# TODO: Fix merge editor config

# Geany ---------------------
echo "Setting up Geany"
cp -R geany ~/.config/Geany


# HexChat -------------------
echo "Setting up HexChat"
cp -R HexChat/config ~/.config/hexchat


# XCFE Config ------------------
# TODO...

# Thunar (XCFE filebrowser) ---
echo "Setting up Thunar"
cp -R Thunar ~./config/Thunar

